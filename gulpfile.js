var gulp    = require( 'gulp' ),
    server  = require( 'gulp-develop-server' ),
    webpack = require( 'webpack-stream' );

gulp.task('server:start', function() {
  server.listen({ path: './server.js' });
});

gulp.task('server:restart', function() {
  gulp.watch(['./server.js'], server.restart);
});

gulp.task('default', ['server:start', 'server:restart']);