const INIT_STATE = {
  allOrders: {},
  choicedOrder: {}
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case 'FETCH_ORDERS_SUCCESS':
      return { ...state, allOrders: { ...action.payload } };
    case 'LOAD_ORDER':
      return {
        ...state,
        choicedOrder: action.payload
      };
    default:
      return state;
  }
};
