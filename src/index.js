import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import ReduxThunk from 'redux-thunk';
// components
import OrdersList from './components/OrdersList';
import OrderPage from './components/OrderPage';
import Main from './components/Main';

import reducers from './reducers';

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path='/' component={Main}>
        <Route path='orders' component={OrdersList} />
        <Route path='orders/:orderId' component={OrderPage} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('app')
);
