import firebase from 'firebase';

export const loadOrders = () => {
  return dispatch => {
    let feed = [];
    firebase.database().ref('/orders')
      .on('value', snapshot => {
        dispatch({
          type: 'FETCH_ORDERS_SUCCESS',
          payload: snapshot.val() || {}
        });
      });
  };
};

export const choiceOrder = id => {
  return dispatch => {
    firebase.database().ref(`/orders/${id}`)
      .on('value', snapshot => {
        dispatch({
          type: 'LOAD_ORDER',
          payload: snapshot.val()
        });
      })
  };
};
