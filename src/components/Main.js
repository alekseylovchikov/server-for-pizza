import React from 'react';
import firebase from 'firebase';
import { Link } from 'react-router';

class Main extends React.Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyB5Kn5ayAJIMPhvzEZphsI1d0GIERuUuWA',
      authDomain: 'antonio-pizza.firebaseapp.com',
      databaseURL: 'https://antonio-pizza.firebaseio.com',
      storageBucket: 'antonio-pizza.appspot.com',
      messagingSenderId: '501179292149'
    });
  }
  render() {
    return (
      <div>
        <div className='jumbotron'>
          <h1 style={{ textAlign: 'center', color: '#fff' }}>Заказы</h1>
        </div>
        <div className='container'>
          <nav>
            <ul style={{ listStyle: 'none', display: 'flex', justifyContent: 'space-around', padding: 0 }}>
              <li><Link to='/'>Главная</Link></li>
              <li><Link to='/orders'>Заказы</Link></li>
            </ul>
          </nav>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Main;
