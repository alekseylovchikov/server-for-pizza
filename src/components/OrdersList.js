import React, { Component } from 'react';
import { connect } from 'react-redux';
import Push from 'push.js';

import { loadOrders } from '../actions';

import OrderItem from './OrderItem';

const IMAGE_URL = 'https://pp.userapi.com/c837631/v837631879/225af/Be4nZBTZZ80.jpg';

class OrdersList extends Component {
  componentWillMount() {
    this.props.loadOrders();
  }
  componentWillReceiveProps(nextProps) {
    if (Object.keys(nextProps.orders).length !== Object.keys(this.props.orders).length && Object.keys(this.props.orders).length > 0) {
      console.log(nextProps.orders, this.props.orders);
      const orders = Object.keys(nextProps.orders);
      const lastOrder = nextProps.orders[orders[orders.length - 1]];
      Push.create('Новый заказ!', {
        body: `Адрес доставки: ${lastOrder.address}\nСумма заказа: ${lastOrder.totalAmount} рублей\nТелефон: ${lastOrder.phone}\nИмя клиента: ${lastOrder.name}\nКоличество товаров: ${lastOrder.itemsInCart.length}`,
        icon: IMAGE_URL,
        onclick: () => {
          window.focus();
          this.close();
        }
      });
    }
  }
  render() {
    let orders = [];
    const keys = Object.keys(this.props.orders).length > 0 ? Object.keys(this.props.orders) : [];
    if (keys.length) {
      keys.map(key => orders.push(this.props.orders[key]));
    }
    return (
      orders.length > 0 ?
      <table className='table table-striped table-bordered table-hover'>
        <thead>
          <tr>
            <th>Номер заказа</th>
            <th>Дата заказа</th>
            <th>Товары в корзине</th>
            <th>Сумма заказа</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order, i) => <OrderItem key={i} {...order} id={keys[i]} orderN={++i} />)}
        </tbody>
      </table> :
      <div className="main-loading">
        <div className="dot"></div>
        <div className="dot"></div>
        <div className="dot"></div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  orders: state.orders.allOrders
});

export default connect(mapStateToProps, { loadOrders })(OrdersList);
