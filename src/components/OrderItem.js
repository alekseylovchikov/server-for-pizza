import React from 'react';
import { Link } from 'react-router';

export default ({ address, createdAt, itemsInCart, name, phone, totalAmount, id, orderN }) => {
  return (
    <tr>
      <td>
        <Link to={`/orders/${id}`}>Заказ №{orderN}</Link>
      </td>
      <td>{createdAt}</td>
      <td>
        {itemsInCart.map((item, i) => {
          return <div key={i}>{item.title}</div>;
        })}
      </td>
      <td>{totalAmount} рублей</td>
    </tr>
  );
};
