import React from 'react';
import { connect } from 'react-redux';
import { choiceOrder } from '../actions';

class OrderPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      prop: this.props.params.orderId
    };
  }
  componentWillMount() {
    this.props.choiceOrder(this.state.prop);
  }
  render() {
    const { choicedOrder } = this.props;
    return (
      Object.keys(choicedOrder).length > 0 ?
      <div className='well'>
        <h4><i className="fa fa-user" aria-hidden="true"></i> Имя клиента: {choicedOrder.name}</h4>
        <h4><i className="fa fa-map-marker" aria-hidden="true"></i> Адрес доставки: {choicedOrder.address}</h4>
        <h4><i className="fa fa-phone" aria-hidden="true"></i> Номер: {choicedOrder.phone}</h4>
        <h4><i className="fa fa-money" aria-hidden="true"></i> Сумма заказа: {choicedOrder.totalAmount} рублей</h4>
        <div>
          <h4>
            <i className="fa fa-shopping-cart" aria-hidden="true"></i> Товары в корзине:
          </h4>
          <ul>
            {choicedOrder.itemsInCart.map((item, i) => <li key={i}>{item.title}, количество: {item.count}</li>)}
          </ul>
        </div>
      </div> :
      <div className="main-loading">
        <div className="dot"></div>
        <div className="dot"></div>
        <div className="dot"></div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  choicedOrder: state.orders.choicedOrder
});

export default connect(mapStateToProps, { choiceOrder })(OrderPage);
