const express = require('express');
const path = require('path');
const morgan = require('morgan');
const app = express();
const axios = require('axios');
const WebpackDevMiddleware = require('webpack-dev-middleware');
const WebpackHotMiddleware = require('webpack-hot-middleware');
const webpack = require('webpack');

const PORT = process.env.PORT || 3000;
const webpackConfig = require('./webpack.config');

const compiler = webpack(webpackConfig, function(err, stats) {
  if (err) { throw err; }
  console.log(stats.toString({
    colors: true,
    children: false,
    chunks: false,
    modules: false
  }));
});

const devMiddleware = WebpackDevMiddleware(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath
});

const hotMiddleware = WebpackHotMiddleware(compiler);

app.use(devMiddleware);
app.use(hotMiddleware);
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));

// app use static files
app.use('/js', express.static(__dirname + '/public/js'));
app.use('/css', express.static(__dirname + '/public/css'));

app.get('*', (req, res) => {
  res.sendFile('index.html', { root: __dirname + '/public' });
});

// run server
app.listen(PORT, err => {
  if (err) {
    console.log("***************************");
    console.log(err);
    console.log("***************************");
  } else {
    console.log('===========================');
    console.log(`Server run: http://localhost:${PORT}`);
    console.log('===========================');
  }
});
